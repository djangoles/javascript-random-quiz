let container = document.getElementById('container')
const startBtn = document.getElementById('start-btn')
const questionsContainer = document.getElementById('questions-container')
const questions = document.getElementById('questions')
const answersDIV = document.getElementById('answers')
const results = document.querySelector('.results')
const playAgain = document.getElementById('play-again')
questionsContainer.style.display = 'none';

const url = 'https://opentdb.com/api.php'


let gameState = {
    quiz: [],
    correctAnswer: '',
    userAnswer: '',
    quizCounter: 0,
    guessesLeft: 0,
    correctGuesses: 0
}

const loadGame = () => {
    startBtn.style.display = 'none'
    questionsContainer.style.display = 'block'
    gameState.guessesLeft = gameState.quiz.length
    const question = getSingleQuestion()
    addQuestionToPage(question)
}

const unescapeHtml = (safe) => {
    return safe.replace(/&amp;/g, '&')
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&quot;/g, '"')
        .replace(/&#039;/g, "'");
}

const addQuestionToPage = (singleQuestion) => {
    answersDIV.innerHTML = ''
    const question = singleQuestion.question
    const ul = document.createElement('ul')
    ul.classList.add('answers-list')
    gameState.correctAnswer = unescapeHtml(singleQuestion.correct_answer);
    let answers

    if(singleQuestion.type === 'boolean') {
        answers = [gameState.correctAnswer, singleQuestion.incorrect_answers[0]]
    } else {
        answers = [
            unescapeHtml(singleQuestion.correct_answer), 
            unescapeHtml(singleQuestion.incorrect_answers[0]),
            unescapeHtml(singleQuestion.incorrect_answers[1]),
            unescapeHtml(singleQuestion.incorrect_answers[2])
        ]
    }
    let randomIndexArr = []
    while(randomIndexArr.length < answers.length) {
        let randNum = Math.floor(Math.random() * answers.length)
        if(!randomIndexArr.includes(randNum)) {
            randomIndexArr.push(randNum)
        }       
    }
    answers.forEach((element, i, arr) => {
       let li = document.createElement('li')
       li.classList.add('answer')
       li.addEventListener('click', handleUserAnswerClick)
       let input = document.createElement('input')
       li.innerHTML = `<input type="checkbox">${answers[randomIndexArr[i]]}`
       ul.appendChild(li)
    });
    questions.innerHTML = `
    <h3 class="category">${singleQuestion.category}</h3>
    <p class="question">${question}</p>`
    answersDIV.appendChild(ul)
}

const getSingleQuestion = () => {
    let currentQuestionNum = gameState.quizCounter
    gameState.quizCounter++
    return gameState.quiz[currentQuestionNum]
}

const reStartGame = () => {
    gameState.correctAnswer = '',
    gameState.userAnswer = '',
    gameState.quizCounter = 0,
    gameState.guessesLeft = 0,
    gameState.correctGuesses = 0
    results.classList.remove('results-show')
    const question = getSingleQuestion()
    addQuestionToPage(question)
}


const handleUserAnswerClick = (e) => {

    if(e.target.type === 'checkbox'){
        const lis = document.querySelectorAll('.answer')
        // DISABLE INPUTS
        lis.forEach(element => {
        let input = element.firstElementChild
        input.disabled = true
        });
        // GET USER SELECTION AND ADD TO STATE
        const userAnswer = e.target.parentElement.textContent
        gameState.userAnswer = userAnswer
        console.log(`USER: ${userAnswer} - Correct ${gameState.correctAnswer}`)
        // CHECK CORRECT VS USER
        if(gameState.userAnswer === gameState.correctAnswer) {
            gameState.correctGuesses++
            lis.forEach(element => {
                if(element.textContent ===  gameState.userAnswer) {
                    element.classList.add('answer-correct')
                    element.style.color = "#ffffff"
                    element.innerHTML = `<p>${gameState.userAnswer} - <span class="span-correct">correct </span></p>`
                } 
            });
        } else {
            lis.forEach(element => {
                if(element.textContent ===  gameState.userAnswer) {
                    element.classList.add('answer-incorrect')
                    element.style.color = "#ffffff"
                    element.innerHTML = `<p>${gameState.userAnswer} - <span class="span-incorrect">incorrect </span></p>`
                } else if(element.textContent ===  gameState.correctAnswer) {
                    element.classList.add('answer-correct')
                    element.style.color = "#ffffff"
                    element.innerHTML = `<p>${gameState.correctAnswer} - <span class="span-correct">correct </span></p>`
                } 
            });
        }

        // Show ANSWER THEN RUN FINAL CHECKS
        if(gameState.quizCounter === gameState.quiz.length) {
            setTimeout(() => {
                let finalUserResult = document.querySelector('.final-user-result')
                finalUserResult.textContent = `You got ${gameState.correctGuesses} question(s) correct`
                results.classList.add('results-show')
            }, 500)
        } else {
            let nextBTN = document.createElement('button')
            nextBTN.classList.add('next-btn')
            nextBTN.textContent = 'Next'
            answersDIV.appendChild(nextBTN)  
        }   
    } 
}

playAgain.addEventListener('click', () => {
    axios.get(url,{
        params: {
            amount: 4
        }
    })
    .then(function(response) {
        gameState.quiz = response.data.results
        if(gameState.quiz.length > 0) {
            reStartGame()
        }
        console.log(gameState.quiz)
    })
    .catch(function(err) {
        console.log(err)
    })  
})


answersDIV.addEventListener('click', (e) => {
    if(e.target.classList.contains('next-btn')) {
        const question = getSingleQuestion()
        addQuestionToPage(question)
    }
})

window.addEventListener('load', () => {
    axios.get(url,{
        params: {
            amount: 4
        }
    })
    .then(function(response) {
        gameState.quiz = response.data.results
        if(gameState.quiz.length > 0) {
            startBtn.addEventListener('click', loadGame)
        }
        console.log(gameState.quiz)
    })
    .catch(function(err) {
        console.log(err)
    })
})